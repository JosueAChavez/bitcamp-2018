package youtube_live;

import java.io.IOException;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.CdnSettings;
import com.google.api.services.youtube.model.IngestionInfo;
import com.google.api.services.youtube.model.LiveBroadcast;
import com.google.api.services.youtube.model.LiveBroadcastSnippet;
import com.google.api.services.youtube.model.LiveBroadcastStatus;
import com.google.api.services.youtube.model.LiveStream;
import com.google.api.services.youtube.model.LiveStreamListResponse;
import com.google.api.services.youtube.model.LiveStreamSnippet;
import com.google.common.collect.Lists;

import manager.LiveStreamEvent;
import manager.StreamingPlatform;

public class YouTubeStream implements StreamingPlatform {
    /**
     * Define a global instance of a YouTube object, which will be used
     * to make YouTube Data API requests.
     */
    private YouTube youtube;
    private String streamTitle;

	public YouTubeStream() {
    	streamTitle = "DemoStreamTitle";
    	
	    // This OAuth 2.0 access scope allows for full read/write access to the
        // authenticated user's account.
        java.util.List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");

        // Authorize the request. TODO understand
        Credential credential;
		try {
			credential = Auth.authorize(scopes, "createbroadcast");
	        // This object is used to make YouTube Data API requests. TODO understand
	        youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
	                .setApplicationName("youtube-cmdline-createbroadcast-sample").build();

		} catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
            e.printStackTrace();
		}
    }
    
    public YouTubeStream(String streamTitle) {
    	this();
    	this.streamTitle = streamTitle;
    }

	public void stream(LiveStreamEvent lsEvent) {
		try {
            LiveBroadcast returnedBroadcast = setupBroadcast(lsEvent);
            LiveStream returnedStream = setupStream(streamTitle, returnedBroadcast);
            
            // Construct and execute a request to bind the new broadcast
            // and stream.
            YouTube.LiveBroadcasts.Bind liveBroadcastBind =
                    youtube.liveBroadcasts().bind(returnedBroadcast.getId(), "id,contentDetails");
            liveBroadcastBind.setStreamId(returnedStream.getId());
            returnedBroadcast = liveBroadcastBind.execute();

            // Print information from the API response.
            System.out.println("\n================== Returned Bound Broadcast ==================\n");
            System.out.println("  - Broadcast Id: " + returnedBroadcast.getId());
            System.out.println(
                    "  - Bound Stream Id: " + returnedBroadcast.getContentDetails().getBoundStreamId());

        } catch (GoogleJsonResponseException e) {
            System.err.println("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
            e.printStackTrace();

        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
            e.printStackTrace();
        } catch (Throwable t) {
            System.err.println("Throwable: " + t.getMessage());
            t.printStackTrace();
        }
		
	}

	private LiveBroadcast setupBroadcast(LiveStreamEvent lsEvent) throws IOException {
        // Prompt the user to enter a title for the broadcast.
        String title = lsEvent.getTitle();
        String body = lsEvent.getBody();
//        System.out.println("You chose " + title + " for broadcast title.");

        // Create a snippet with the title and scheduled start and end
        // times for the broadcast. Currently, those times are hard-coded.
        LiveBroadcastSnippet broadcastSnippet = new LiveBroadcastSnippet();
        broadcastSnippet.setTitle(title);
        broadcastSnippet.setDescription(body);
        broadcastSnippet.setScheduledStartTime(new DateTime(lsEvent.getStartTime().toString()));
        broadcastSnippet.setScheduledEndTime(new DateTime(lsEvent.getEndTime().toString()));
        
        // Set the broadcast's privacy status to "private". See:
        // https://developers.google.com/youtube/v3/live/docs/liveBroadcasts#status.privacyStatus
        LiveBroadcastStatus status = new LiveBroadcastStatus();
        status.setPrivacyStatus("unlisted");

        LiveBroadcast broadcast = new LiveBroadcast();
        broadcast.setKind("youtube#liveBroadcast");
        broadcast.setSnippet(broadcastSnippet);
        broadcast.setStatus(status);

        // Construct and execute the API request to insert the broadcast.
        YouTube.LiveBroadcasts.Insert liveBroadcastInsert =
                youtube.liveBroadcasts().insert("snippet,status", broadcast);
        LiveBroadcast returnedBroadcast = liveBroadcastInsert.execute();

        // Print information from the API response.
        printReturnedBroadcast(returnedBroadcast);
        
        return returnedBroadcast;
		
	}
	
    private LiveStream setupStream(String title, LiveBroadcast returnedBroadcast) throws IOException {
        // Prompt the user to enter a title for the video stream.
//        System.out.println("You chose " + title + " for stream title.");

    	LiveStream res = findLiveStream(title);
    	
    	if (res != null) {
            printReturnStream(res);
    		return res;
    	}
        // Create a snippet with the video stream's title.
        LiveStreamSnippet streamSnippet = new LiveStreamSnippet();
        streamSnippet.setTitle(title);

        // Define the content distribution network settings for the
        // video stream. The settings specify the stream's format and
        // ingestion type. See:
        // https://developers.google.com/youtube/v3/live/docs/liveStreams#cdn
        
        IngestionInfo ingestionInfo = new IngestionInfo();
        ingestionInfo.setStreamName("IngestionDemoTitle");
        ingestionInfo.setIngestionAddress("rtmp://184.72.239.149/vod/mp4:bigbuckbunny_1500.mp4");
        ingestionInfo.setBackupIngestionAddress("rtmp://184.72.239.149/vod/mp4:bigbuckbunny_1100.mp4");
        
        CdnSettings cdnSettings = new CdnSettings();
        cdnSettings.setFormat("1080p");
        cdnSettings.setIngestionType("rtmp");
        cdnSettings.setIngestionInfo(ingestionInfo);

        LiveStream stream = new LiveStream();
        stream.setKind("youtube#liveStream");
        stream.setSnippet(streamSnippet);
        stream.setCdn(cdnSettings);

        // Construct and execute the API request to insert the stream.
        YouTube.LiveStreams.Insert liveStreamInsert =
                youtube.liveStreams().insert("snippet,cdn", stream);
        LiveStream returnedStream = liveStreamInsert.execute();

        printReturnStream(returnedStream);
        
        return returnedStream;
	}

	private void printReturnedBroadcast(LiveBroadcast returnedBroadcast) {
        System.out.println("\n================== Returned Broadcast ==================\n");
        System.out.println("  - Id: " + returnedBroadcast.getId());
        System.out.println("  - Title: " + returnedBroadcast.getSnippet().getTitle());
        System.out.println("  - Description: " + returnedBroadcast.getSnippet().getDescription());
        System.out.println("  - Published At: " + returnedBroadcast.getSnippet().getPublishedAt());
        System.out.println(
                "  - Scheduled Start Time: " + returnedBroadcast.getSnippet().getScheduledStartTime());
        System.out.println(
                "  - Scheduled End Time: " + returnedBroadcast.getSnippet().getScheduledEndTime());
	}

	private void printReturnStream(LiveStream returnedStream) {
        // Print information from the API response.
        System.out.println("\n================== Returned Stream ==================\n");
        System.out.println("  - Id: " + returnedStream.getId());
        System.out.println("  - Title: " + returnedStream.getSnippet().getTitle());
        System.out.println("  - Description: " + returnedStream.getSnippet().getDescription());
        System.out.println("  - Published At: " + returnedStream.getSnippet().getPublishedAt());
	}
	
	/**
	 * Finds live stream
	 * @param title
	 * @return LiveStream if its found. null otherwise.
	 */
	private LiveStream findLiveStream(String title) {
		 java.util.List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube.readonly");

	        try {
	            // Authorize the request.
	            Credential credential = Auth.authorize(scopes, "createbroadcast");

	            // This object is used to make YouTube Data API requests.
	            youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
	                    .setApplicationName("youtube-cmdline-liststreams-sample")
	                    .build();

	            // Create a request to list liveStream resources.
	            YouTube.LiveStreams.List livestreamRequest = youtube.liveStreams().list("id,snippet");

	            // Modify results to only return the user's streams.
	            livestreamRequest.setMine(true);

	            // Execute the API request and return the list of streams.
	            LiveStreamListResponse returnedListResponse = livestreamRequest.execute();
	            java.util.List<LiveStream> returnedList = returnedListResponse.getItems();
	            for (LiveStream stream : returnedList) {
	            	if(stream.getSnippet().getTitle().equals(title)) {
	            		return stream;
	            	}
	            }
	            // Print information from the API response.
	            /*System.out.println("\n================== Returned Streams ==================\n");
	            for (LiveStream stream : returnedList) {
	                System.out.println("  - Id: " + stream.getId());
	                System.out.println("  - Title: " + stream.getSnippet().getTitle());
	                System.out.println("  - Description: " + stream.getSnippet().getDescription());
	                System.out.println("  - Published At: " + stream.getSnippet().getPublishedAt());
	                System.out.println("\n-------------------------------------------------------------\n");
	            }*/

	        } catch (GoogleJsonResponseException e) {
	            System.err.println("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
	                    + e.getDetails().getMessage());
	            e.printStackTrace();

	        } catch (IOException e) {
	            System.err.println("IOException: " + e.getMessage());
	            e.printStackTrace();
	        } catch (Throwable t) {
	            System.err.println("Throwable: " + t.getMessage());
	            t.printStackTrace();
	        }
	        
	        return null;
	}
	
    public String getStreamTitle() {
		return streamTitle;
	}

	public void setStreamTitle(String streamTitle) {
		this.streamTitle = streamTitle;
	}
}
