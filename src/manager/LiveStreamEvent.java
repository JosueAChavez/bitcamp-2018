package manager;

import java.time.Clock;
import java.time.LocalDateTime;

public class LiveStreamEvent {
	
	private String title, body;
	private LocalDateTime startTime, endTime;	
	
	public LiveStreamEvent(String title, String body) {
		this.title = title;
		this.body = body;
		
		startTime = LocalDateTime.now(Clock.systemUTC());
		endTime = startTime.plusHours(1);
	}
	
	public void setStartTime(int hour, int minute) {
		startTime = LocalDateTime.of(startTime.getYear(), startTime.getMonth(), 
				startTime.getDayOfMonth(), hour, minute);
	}
	
	public void setEndTime(int hour, int minute) {
		endTime = LocalDateTime.of(endTime.getYear(), endTime.getMonth(), 
				endTime.getDayOfMonth(), hour, minute);
	}
	
	public void setStartDate(int month, int day, int year) {
		startTime = LocalDateTime.of(year, month, day, startTime.getHour(), startTime.getMinute());
	}
	
	public void setEndDate(int month, int day, int year) {
		endTime = LocalDateTime.of(year, month, day, endTime.getHour(), endTime.getMinute());
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getBody() {
		return body;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public LocalDateTime getEndTime() {
		return endTime;
	}
}
