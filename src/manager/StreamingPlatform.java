package manager;

public interface StreamingPlatform {
	public void stream(LiveStreamEvent lsEvent);
}