package manager;

import facebook_live.FacebookStream;
import youtube_live.YouTubeStream;

public class LiveStreamManager {

	private static StreamingPlatform ytStream = new YouTubeStream();
	private static StreamingPlatform fbStream = new FacebookStream();
	
	public static void streamYouTube(LiveStreamEvent lsEvent) {
		ytStream.stream(lsEvent);
	}
	
	public static void streamFacebook(LiveStreamEvent lsEvent) {
		fbStream.stream(lsEvent);
	}
}
