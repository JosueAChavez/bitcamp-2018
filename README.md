![alt text](logo.png "Bitcamp Logo")
# AIO Livestream
For Bitcamp 2018 (hackathon), my friend and I made a program for setting up live streams platform all in one place. This uses the YouTube Live Streaming [API](https://developers.google.com/youtube/v3/live/getting-started) (also known as YouTube Data API v3) and Facebook Live [API](https://developers.facebook.com/docs/videos/live-video/).


## Purpose
The purpose of this project was to allow users to go to a single place for setting up live streams as oppose to setting them up individually.


## Goals
In addition to the purpose, the goals were aiming for in doing this project was to:
###
- Get experience working with API's
- Reading API documentation and researching
- Incorporate good programming design in a real project (interfaces, folders)
- Get working knowledge using OAuth and using tokens given by API developers 
- Gain more familiarity  with Java's built-in libraries (LocalTimeDate, FileInputStream, etc.) 
- Version controlling with multiple people involved